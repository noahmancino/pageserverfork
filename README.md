Author: Noah Mancino
Email: noahdanielmancino@gmail.com

A very basic web server. Useful for sending HTML files to localhost. 
Will serve 404 and 403 when appropriate (in the case of 403, 
this means any request for a file above DOCROOT).
